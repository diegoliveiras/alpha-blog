class Article < ActiveRecord::Base
	belongs_to :user
	has_many :article_categories
	has_many :categories, through: :article_categories

	validates :title, presence: true, length: { minimum: 3, maximum: 50 }
	validates :description, presence: true, length: { minimum: 10, maximum: 1000 }
	validates :user_id, presence: true
	validates :categories, presence: true
	
	def self.to_csv
		CSV.generate do |csv|
		  csv << column_names
		  all.each do |article|
		    csv << article.attributes.values_at(*column_names)
		  end
		end
	end

end